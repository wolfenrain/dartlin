import 'package:flutter_test/flutter_test.dart';
import 'package:dartlin/collections.dart';

void main() {
  test('Should consider pairs to be equal according to its elements', () {
    expect(Pair(1, 2), Pair(1, 2));
    expect(Pair(1, 'a') == Pair(1, 'a'), true);
    expect(Pair(1, 2) == Pair(1, 3), false);
    expect(Pair(1, 'a') == Pair(1, 'b'), false);
  });
}
