import 'package:flutter_test/flutter_test.dart';
import 'package:dartlin/collections.dart';

void main() {
  test('Should return empty list for list with length < 2', () {
    final list1 = [];
    expect(list1.zipWithNext(), []);
    expect(list1.zipWithNext(loopAround: false), []);

    final list2 = [1];
    expect(list2.zipWithNext(), []);
    expect(list2.zipWithNext(loopAround: false), []);
  });

  test('Should return single pair when list has two elements', () {
    final list = [1, 2];
    expect(list.zipWithNext(), [Pair(1, 2)]);
  });

  test(
    'Should return two pairs when list has two elements and loopAround is true',
    () {
      final list = [1, 2];
      expect(list.zipWithNext(loopAround: true), [Pair(1, 2), Pair(2, 1)]);
    },
  );

  test('Should return multiple pairs when list has multiple elements', () {
    final list = [1, 2, 3, 4];
    expect(list.zipWithNext(), [Pair(1, 2), Pair(2, 3), Pair(3, 4)]);
    expect(
      list.zipWithNext(loopAround: true),
      [Pair(1, 2), Pair(2, 3), Pair(3, 4), Pair(4, 1)],
    );
  });
}
