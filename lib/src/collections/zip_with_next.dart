part of collections;

extension ZipWithNext<T> on List<T> {
  /// Returns a list of pairs of each two adjacent elements in this list.
  ///
  /// The returned list is empty if this collection contains less than two
  /// elements.
  List<Pair<T, T>> zipWithNext({
    /// If set, a final pair from the last element back to the first will be
    /// considered.
    bool loopAround = false,
  }) {
    if (length <= 1) {
      return [];
    }
    final results = <Pair<T, T>>[];
    for (var index = 0; index < length - 1; index++) {
      final t1 = this[index];
      final t2 = this[index + 1];
      results.add(Pair(t1, t2));
    }
    if (loopAround) {
      results.add(Pair(last, first));
    }
    return results;
  }
}
