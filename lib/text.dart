/// Adding methods for text control.
library text;

import 'package:dartlin/collections.dart';

export 'package:dartlin/collections.dart' show Pair;

part 'src/text/associate.dart';
part 'src/text/chunked.dart';
part 'src/text/grouping_by.dart';
